#!/bin/bash

user=`whoami`
tasks_file="/home/$user/.mosquito-notes.tsk"
notification_icon_norm="/usr/share/icons/hicolor/scalable/apps/mate-notification-properties1.svg"

set_dbus() {
    pids=`pgrep -u $user bash`
    for pid in $pids; do
        # find DBUS session bus for this session
        DBUS_SESSION_BUS_ADDRESS=`grep -z DBUS_SESSION_BUS_ADDRESS /proc/$pid/environ | sed -e 's/DBUS_SESSION_BUS_ADDRESS=//'`
        # use it
        export DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS
    done
}

get_display () {
    who | grep $user | perl -ne 'if ( m!\(\:(\d+)\)$! ) {print ":$1.0\n"; $ok = 1; last} END {exit !$ok}'
}

if ! [ -f $tasks_file ];
then touch $tasks_file;
fi

case $1 in
# add new task to tasks list
'-a') {
    msg=$@;
    msg=${msg:3};
    awk_arg="BEGIN { found = 0; prev_task = 0; };
                   { if ( \$1 - prev_task > 1) { found = 1; nextfile; }; prev_task = \$1; };
               END { print found, FNR, prev_task+1; };";
    sed_arg=(`awk "$awk_arg" $tasks_file`);
    level="norm";
    if [ ${sed_arg[0]} == "0" ] ;
        then { echo "${sed_arg[2]} $level $msg" >> $tasks_file; };
        else { sed -i "${sed_arg[1]}"i\ "${sed_arg[2]} $level $msg" $tasks_file; };
    fi
} ;;

# print help info
'-h') {
    echo 'reminder -[ahlrp]';
} ;;

# print all tasks and their numbers to stdout
'-l') {
    case ${2:-'norm'} in
        'all') { cat $tasks_file; } ;;
        'low') { cat $tasks_file | grep low; } ;;
        'norm') { cat $tasks_file | grep norm; } ;;
        'high') { cat $tasks_file | grep high; } ;;
    esac
} ;;

# run script in bg mode
'-p') {
    if [ -z "$DBUS_SESSION_BUS_ADDRESS" ]; then
        set_dbus
    fi
    export DISPLAY=$(get_display);

    header='Current tasks'
    timeout=$((`cat $tasks_file | wc -l` * 200 + 5000));
    awk_arg="{ for (i = 3; i < NF; i++) printf(\"%s \", \$i); print \$i}";
    body=`awk "$awk_arg" $tasks_file`;
    /usr/bin/notify-send -i "$notification_icon_norm" -t $timeout "$header" "$body";
} ;;

# remove task with taken number
'-r') {
    awk_arg="\$1 == "$2" { print FNR }";
    sed_arg=`awk "$awk_arg" $tasks_file`;
    if [ -z $sed_arg ] ;
        then { echo "Task #$2 not found."; } ;
        else { echo "Remove task #$2."; sed -i "$sed_arg"d $tasks_file; } ;
    fi
} ;;

esac

